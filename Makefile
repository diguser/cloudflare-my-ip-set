CC = g++
LD = g++

DIR_SRC = src
DIR_OBJ = obj
DIR_BIN = bin
DIR_HEADER = header
DIR_RES = res

ifeq ($(OS),Windows_NT)
  WIN=1
  RM =del /s /f /q
  FILE_DIV =/
  FILE_DIV2=\\
  EXEC_EXTENSION =.exe
  COLOR_BLUE =
  COLOR_GREEN=
  COLOR_RED  =
  COLOR_DEF  =
  INC_CURL=-I../../lib/curl/include
  LIB_CURL=-L../../lib/curl/lib64 -lcurldll
  INC_JSON=-I../../lib/libjson
  LIB_JSON=-L../../lib/libjson -ljson
  TARGET=WIN
else
  WIN=0
  RM =rm -fr
  FILE_DIV =/
  EXEC_EXTENSION =
  COLOR_BLUE =\033[1;34m
  COLOR_GREEN=\033[1;32m
  COLOR_RED  =\033[31m
  COLOR_DEF  =\033[0m
  INC_CURL=
  LIB_CURL=-lcurl
  INC_JSON=-I../../lib/libjson
  LIB_JSON=-L../../lib/libjson -ljson
endif

ifeq (x86_64,$(findstring x86_64,$(shell $(CC) -dumpmachine)))
  IUPMANIFEST = iup64.manifest
else
  IUPMANIFEST = iup32.manifest
endif

CCFLAGS =-Wall -std=gnu++11 $(INC_CURL) $(INC_JSON)
LDFLAGS =-Wall $(LIB_CURL) $(LIB_JSON)

HEADERS = $(wildcard $(DIR_HEADER)$(FILE_DIV)*.hpp)
DIRS = $(DIR_OBJ) $(DIR_BIN)


EX1_SRC =\
main.cpp \
Error.cpp \
Web.cpp

EX1_OUT = run
EX1_SRC_WPATH =$(addprefix $(DIR_SRC)$(FILE_DIV),$(EX1_SRC))
EX1_OBJ_WPATH =$(addprefix $(DIR_OBJ)$(FILE_DIV),$(patsubst %.cpp,%.o,$(EX1_SRC)))
EX1_OUT_WPATH =$(DIR_BIN)$(FILE_DIV)$(EX1_OUT)$(EXEC_EXTENSION)

all: $(EX1_OUT)

$(EX1_OUT) $(EX1_OUT_WPATH): $(DIRS) $(EX1_OBJ_WPATH)
ifeq ($(WIN),0)
	-@echo "$(COLOR_BLUE)Linking$(COLOR_GREEN)    $(EX1_OUT_WPATH)$(COLOR_RED)"
	-@$(RM) $(EX1_OUT_WPATH)
	-@$(LD) $(LDFLAGS) $(EX1_OBJ_WPATH) $(LDFLAGS) -o $(EX1_OUT_WPATH)
	@if [ -f $(EX1_OUT_WPATH) ]; then echo "\033[1;32m           Successfully\033[0m"; else echo "Linking    ERROR\033[0m"; return 1; fi;
else
	-@echo Linking    $(EX1_OUT_WPATH)
	@$(LD) $(LDFLAGS) $(EX1_OBJ_WPATH) $(LDFLAGS) -o $(EX1_OUT_WPATH)
	-@echo            Successfully
endif
	

$(DIR_RES)/gui_res.o:
	-@echo Resource
	-@copy $(DIR_RES)\$(IUPMANIFEST) $(DIR_RES)\iup.manifest /y >> nul
	-@windres $(DIR_RES)/iup.rc $(DIR_RES)/gui_res.o
	-@del $(DIR_RES)\iup.manifest

$(DIRS):
	-@mkdir $@

$(DIR_OBJ)$(FILE_DIV)%.o: $(DIR_SRC)$(FILE_DIV)%.cpp $(HEADERS)
ifeq ($(WIN),0)
	-@echo "$(COLOR_BLUE)Compile$(COLOR_GREEN)    $<$(COLOR_RED)"
	-@$(RM) $@
	-@$(CC) $(CCFLAGS) -c $< -o $@
	@if [ -f $@ ]; then return 0; else echo "Compile    ERROR\033[0m"; return 1; fi;
else
	-@echo Compile    $<
	@$(CC) $(CCFLAGS) -c $< -o $@
endif

color:
	-@echo $(COLOR_DEF)

clean:
ifeq ($(WIN),0)
	-@echo "$(COLOR_BLUE)Cleaning project$(COLOR_RED)"
	-$(RM) $(EX1_OUT_WPATH)
	-$(RM) $(DIR_OBJ)
	-@echo "$(COLOR_DEF)"
else
	-@echo Cleaning project
	-@$(RM) $(subst /,\\,$(EX1_OUT_WPATH))
	-@$(RM) $(subst /,\\,$(DIR_OBJ))
endif
