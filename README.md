## About it
Simple program to get your public IP (internet) and modify (if necessary) one subdomain in Cloudflare to your ip.

You need one Cloudflare accout and (at least) one domain registred in Cloudflare.

Create one subdomain in Cloudflare with type A or AAAA, set name with something you want and set your IP or other public IP.

The application will change that IP with your public IP every time you run the application and the actual public IP if the actual public IP is not equal to the public IP obtained in last application run.

Actualy running in Windows. Tests with Linux will start in some time..

## Needed libraries
- libcurl
- libjson (https://sourceforge.net/projects/libjson/)

## Before compile
Create a file "./header/CD_KEY.h" with this five defines:
**CD_KEY**, **CD_EMAIL**, **CD_HOST**, **CD_DNS_TYPE** and **CD_DNS_NAME**.

All defines are in *C-ANSI string* format.

- **CD_KEY**: Is the *API Key* generated in Cloudflare user settings.
- **CD_EMAIL**: Is the user e-mail used to login in Cloudflare (same as used to generate the key).
- **CD_HOST**: Is the domain name. Use the exacly text showed in domain list in Cloudflare page.
- **CD_DNS_TYPE**: Is the DNS type to modify. (for this code, use A for ipv4 or AAAA for ipv6).
- **CD_DNS_NAME**: The name in DNS list which you want to update with your actual ip.

## Compile
Default usage of *make*..

Maybe you will need to change four macros. You can edit the *Makefile* or use the command-line to it.

- **INC_CURL**: The path for include files of libcurl.
- **LIB_CURL**: The path for libraries files of libcurl and -lcurl
- **INC_JSON**: The path for include files of libjson.
- **LIB_JSON**: The path for libraries files of libjson and -ljson

## Usage

Just call "./bin/run".

If you add parameter *--debug*, some informations will be printed. Otherwise, nothing will appear.