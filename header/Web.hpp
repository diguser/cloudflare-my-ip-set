#ifndef _WEB_HPP_
#define _WEB_HPP_ 1

#include "Error.hpp"
#include <vector>
#include <string>

namespace Web {

Err getPage(std::string&,const std::string,const unsigned=1000);
Err getPage(std::string&,const std::string,const unsigned,const std::vector<std::string>);
Err putPage(std::string&,const std::string,const unsigned,const std::vector<std::string>,const std::string);

}

#endif