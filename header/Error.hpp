#ifndef _ERRORS_HPP_
#define _ERRORS_HPP_ 1

#include <string>
#include <ostream>

  namespace Error {
    int const OK =                        0;
    int const GENERIC_ERROR =            -1;
    int const WSA_ERROR =                -2;
    int const NOT_IMPLEMENTED =          -3;
    int const PTHREAD_ERROR =            -4;
    int const INVALID_STATE =            -5;
    int const MY_SOCKET_ERROR =          -6;
    int const UNABLE_TO_JOIN_GROUP =     -7;
    int const INVALID_VALUE_TYPE =       -8;
    int const INVALID_DATA_RAW_LENGTH =  -9;
    int const DATA_STORAGE_INSUFICIENT =-10;
    int const UNABLE_TO_OPEN_FILE =     -11;
    int const UNABLE_TO_READ_FILE =     -12;
    int const CORRUPTED_FILE =          -13;
    int const CONFIG_DIR_NOT_FOUND =    -14;
    int const CONFIG_FILE_NOT_FOUND =   -15;
    int const NOT_INITIALIZED =         -16;
    int const TITLE_CONFIG_NOT_FOUND =  -17;
    int const INTERNAL_ERROR_DECODING_ELEMENTS =  -18;
    int const ADVANCED_TITLE_CONFIG_NOT_FOUND =   -19;
    int const CURL_FAILED_TO_INIT =     -20;
    int const CURL_HTTP_ERROR =         -21;
    int const CURL_UNKNOWN_ERROR =      -1000;
    int const CURL_TIMEDOUT =           -23;
    int const INVALID_PAGE_CONTENT =    -24;
  }

  class Err {
    private:
      int code;
      std::string privDescription;
      std::string genericDescription;

    public:
      friend std::ostream& operator<<(std::ostream&, const Err&);

      int getCode();
      std::string getPrivateDescription();
      std::string getGenericDescription();

      Err(int ncode, std::string, std::string);
      Err(int, std::string);
      Err(int);
      Err();
  };

#endif
