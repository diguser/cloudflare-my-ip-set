#include "../header/Error.hpp"

int Err::getCode(){
  return code;
}

std::string Err::getPrivateDescription(){
  return privDescription;
}

std::string Err::getGenericDescription(){
  return genericDescription;
}

Err::Err(int errorCode, std::string privateDescription, std::string genericDescription){
  this->code = errorCode;
  this->privDescription = privateDescription;
  this->genericDescription = genericDescription;
}

Err::Err(int errorCode, std::string privateDescription){
  this->code = errorCode;
  this->privDescription = privateDescription;
  this->genericDescription = "";
}

Err::Err(int errorCode){
  this->code = errorCode;
  this->privDescription = "";
  this->genericDescription = "";
}

Err::Err(){
  this->code = 0;
  this->privDescription = "";
  this->genericDescription = "";
}

std::ostream& operator<<(std::ostream &strm, const Err &a){
  //std::stringstream ss;
  if(a.genericDescription.length() != 0)
    return strm << "Error #(" << a.code << ") : " << a.genericDescription << " (" << a.privDescription  << ")";
  return strm << "Error #(" << a.code << ") : " << a.privDescription;
}