#include "../header/Web.hpp"
#include <curl/curl.h>
#include <cstring>


namespace Web {

size_t write_to_string(void* ptr,size_t size,size_t count,void* stream){
  if(stream != NULL)
    ((std::string*)stream)->append((char*)ptr,size*count);
  return size*count;
}

size_t read_callback(char *buffer, size_t size, size_t nitems, void *instream){
  try{
    memcpy(buffer,instream,size*nitems);
    return size*nitems;
  }catch(...){
    return 0;
  }
}

Err getPage(std::string& data,const std::string url,const unsigned timeout){
  CURL *curl;
  CURLcode res;
  curl = curl_easy_init();
  if(!curl)
    return Err(Error::CURL_FAILED_TO_INIT);
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0); 
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, timeout);
  res = curl_easy_perform(curl);
  long http_code = 0;
  curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
  curl_easy_cleanup(curl);
  if((res == CURLE_OK)&&(http_code==404)){
    return Err(Error::CURL_HTTP_ERROR);
  }
  if(res == CURLE_OK)
    return Err(Error::OK);
  if(res == CURLE_OPERATION_TIMEDOUT)
    return Err(Error::CURL_TIMEDOUT);
  return Err(Error::CURL_UNKNOWN_ERROR*res);
}

Err getPage(std::string& data,const std::string url,const unsigned timeout,const std::vector<std::string> hdr){
  CURL *curl;
  CURLcode res;
  curl = curl_easy_init();
  if(!curl)
    return Err(Error::CURL_FAILED_TO_INIT);
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  if(hdr.size()!=0){
    struct curl_slist *chunk = nullptr;
    chunk = curl_slist_append(chunk, "Accept:");
    for(unsigned i=0;i<hdr.size();i++)
      chunk = curl_slist_append(chunk, hdr[i].c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  }
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0); 
  curl_easy_setopt(curl, CURLOPT_HTTPGET, 1); 
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, timeout);
  res = curl_easy_perform(curl);
  long http_code = 0;
  curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
  curl_easy_cleanup(curl);
  if((res == CURLE_OK)&&(http_code==404)){
    return Err(Error::CURL_HTTP_ERROR);
  }
  if(res == CURLE_OK)
    return Err(Error::OK);
  if(res == CURLE_OPERATION_TIMEDOUT)
    return Err(Error::CURL_TIMEDOUT);
  return Err(Error::CURL_UNKNOWN_ERROR*res);
}

Err putPage(std::string& data,const std::string url,const unsigned timeout,const std::vector<std::string> hdr,const std::string content){
  CURL *curl;
  CURLcode res;
  curl = curl_easy_init();
  if(!curl)
    return Err(Error::CURL_FAILED_TO_INIT);
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  if(hdr.size()!=0){
    struct curl_slist *chunk = nullptr;
    chunk = curl_slist_append(chunk, "Accept:");
    for(unsigned i=0;i<hdr.size();i++)
      chunk = curl_slist_append(chunk, hdr[i].c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  }
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0); 
  curl_easy_setopt(curl, CURLOPT_HTTPGET, 1); 
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, timeout);
  curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
  curl_easy_setopt(curl, CURLOPT_READDATA, content.c_str());
  curl_easy_setopt(curl, CURLOPT_INFILESIZE, content.length());
  curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
  res = curl_easy_perform(curl);
  long http_code = 0;
  curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
  curl_easy_cleanup(curl);
  if((res == CURLE_OK)&&(http_code==404)){
    return Err(Error::CURL_HTTP_ERROR);
  }
  if(res == CURLE_OK)
    return Err(Error::OK);
  if(res == CURLE_OPERATION_TIMEDOUT)
    return Err(Error::CURL_TIMEDOUT);
  return Err(Error::CURL_UNKNOWN_ERROR*res);
}

}