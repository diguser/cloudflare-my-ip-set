#include <iostream>
#include <fstream>
#include "../header/Error.hpp"
#include "../header/Web.hpp"
#include "../header/CD_KEY.h"

#pragma GCC diagnostic ignored "-Wreorder"
#define NDEBUG 1
#include <libjson.h>

#define DEBUG(a) if(debug){ std::cout << "[ ] " << a << std::endl; }
#define ERROR(a) if(debug){ std::cout << "[E] " << a << std::endl; }

int main(int ac, char**av){
  bool debug = false;
  std::string file_ip("ip.inf");
  for(int i=1;i<ac;i++){
    if(std::string("--debug").compare(av[i])==0){
      debug=true;
    }else if(std::string("-f").compare(av[i])==0){
      if(i+1>=ac){
        std::cerr << "Invalid argument parameter for -f" << std::endl;
        return -9;
      }
      file_ip = av[++i];
    }
  }
  
  Err e(Error::OK);
  std::string page;
  unsigned tryes=10;
  for(tryes=10;tryes>0;tryes--){
    page.clear();
    DEBUG("Check IP");
    e = Web::getPage(page,"https://api.ipify.org/?format=json",5000);
    if(e.getCode() != Error::OK){
      if(e.getCode() == Error::CURL_TIMEDOUT){
        DEBUG("  Timeout");
        continue;
      }else{
        ERROR("CURL error");
        return -4;
      }
    }else{
      std::string IP;
      {
        JSONNode c = libjson::parse(page);
        if(c.size() == 0){
          ERROR("Parse failed");
          break;
        }
        IP = (c[0]).as_string();
      }
      
      {
        std::fstream file(file_ip.c_str(),std::fstream::in);
        if(file.good()){
          char oIP[40];
          for(unsigned i=0;i<40;i++)
            oIP[i] = '\0';
          file.read(oIP,40);
          DEBUG("Compare actual IP with old IP");
          if(IP.compare(oIP) == 0){
            DEBUG("Old ip and actual IP matches");
            file.close();
            return 0;
          }
        }
        file.close();
      }
      
      std::vector<std::string> headers;
      headers.push_back(std::string("X-Auth-Email: ")+CD_EMAIL);
      headers.push_back(std::string("X-Auth-Key: ")+CD_KEY);
      headers.push_back(std::string("Content-Type: application/json"));
      
      page.clear();
      DEBUG("Get user Zones in Cloudflare");
      e = Web::getPage(page,"https://api.cloudflare.com/client/v4/zones",3000,headers);
      if(e.getCode() != Error::OK){
        ERROR("  Failed");
        continue;
      }
      
      std::string zone_id;
      std::string dns_id;
      std::string CD_ip;
      JSONNode dns_node;
      {
        JSONNode c = libjson::parse(page);
        JSONNode o;
        c = c["result"].as_array();
        for(JSONNode::const_iterator it=c.begin();it!=c.end();it++){
          o = it->as_node();
          if(o["name"].as_string().compare(CD_HOST) == 0){
            zone_id = o["id"].as_string();
            break;
          }
        }
        if(zone_id.length()==0){
          ERROR("Zone in config not found");
          continue;
        }
        
        page.clear();
        DEBUG("Get selected Zone info in Cloudflare");
        e = Web::getPage(page,std::string("https://api.cloudflare.com/client/v4/zones/")+zone_id+std::string("/dns_records"),3000,headers);
        if(e.getCode()!=Error::OK){
          ERROR("  Failed");
          continue;
        }
        c = libjson::parse(page);
        c = c["result"].as_array();
        for(JSONNode::const_iterator it=c.begin();it!=c.end();it++){
          o = it->as_node();
          if((o["type"].as_string().compare(CD_DNS_TYPE) == 0)&&(o["name"].as_string().compare(CD_DNS_NAME)==0)){
            dns_node = o;
            dns_id = o["id"].as_string();
            CD_ip = o["content"].as_string();
            break;
          }
        }
      }
      if(CD_ip.length() == 0){
        ERROR("Cloudflare zone dns not found");
        return -5;
      }
      if(CD_ip.compare(IP) != 0){
        dns_node["content"] = IP;
        page.clear();
        DEBUG("Set new IP in Cloudflare");
        e = Web::putPage(page,std::string("https://api.cloudflare.com/client/v4/zones/")+zone_id+std::string("/dns_records/")+dns_id,3000,headers,dns_node.write());
        
        if(e.getCode()!=Error::OK)
          continue;
        JSONNode c = libjson::parse(page);
        if(c["success"].as_bool()==false){
          return -2;
        }
      }else{
        DEBUG("Actual IP and Cloudflare's IP match");
      }
      
      {
        DEBUG("Save actual ip in file");
        std::fstream file(file_ip.c_str(),std::fstream::out|std::fstream::trunc);
        file << IP;
        file.close();
      }
      break;
    }
  }
  if(tryes == 0)
    return -1;
  return 0;
}